<?php

namespace App\Http\Controllers;

use App\Models\Empleado;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function noUsuarios()
    {
        $usuarios = User::all();

        $NoUsuarios = Empleado::whereNotIn('id', $usuarios->pluck('empleado_id'))
            ->select('empleado.id',
                DB::raw('CONCAT_WS(" ",empleado.primer_nombre, empleado.segundo_nombre,
                empleado.primer_apellido, empleado.segundo_apellido) as full_name'))
            ->get();

        return response()->json($NoUsuarios);
    }

    public function register(Request $request) {
        $request->validate([
            'usuario' => 'required|email|unique:usuario',
            'password' => 'required|confirmed',
            'rol_id' => 'required',
            'employee_id' => 'required'
        ]);

        $user = new User();
        $user->usuario = $request->usuario;
        $user->password = Hash::make($request->password);
        $user->rol_id = $request->rol_id;
        $user->empleado_id = $request->employee_id;
        $user->save();

        return response()->json([
            "status" => 1,
            "msg" => "¡Registro de usuario exitoso!",
        ]);
    }

    public function login(Request $request)
    {
        $request->validate([
            'usuario' => 'required|email',
            'password' => 'required'
        ]);

        $usuario = $request->usuario;
        $user = User::where("usuario", '=', DB::raw("'".$usuario."'"))
                        ->where('activo', 1)->first();

        if(isset($user->id)){
            if(Hash::check($request->password, $user->password)){
                Auth::login($user);
                $token = $user->createToken('token')->plainTextToken;
                $empleado = Empleado::where('id', $user->empleado_id)->first();
                return response()->json([
                    "status" => 1,
                    "msg" => "Usuario autenticado",
                    "access_token" => $token,
                    "usuario" => $user,
                    "empleado" => $empleado,
                ], 200);

            }else{
                return response()->json([
                    "status" => 0,
                    "msg" => "La contraseña es incorrecta",
                ], 404);
            }
        }else{
            return response()->json([
                "status" => 0,
                "msg" => "Usuario no resgistrado",
            ], 404);
        }
    }

    public function userProfile() {
        return response()->json([
            "status" => 0,
            "msg" => "Acerca del perfil de usuario",
            "data" => auth()->user()
        ]);
    }

    public function logout() {
        auth()->user()->tokens()->delete();

        return response()->json([
            "status" => 1,
            "msg" => "Cierre de Sesión",
        ]);
    }
}
