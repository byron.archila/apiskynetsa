<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\ClienteController;
use App\Http\Controllers\EmpleadoController;
use App\Http\Controllers\SupervisorController;
use App\Http\Controllers\TecnicoController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('login', [AuthController::class, 'login'])->name('login');

Route::group( ['middleware' => ["auth:sanctum"]], function(){
    //USUARIOS
    Route::post('register', [AuthController::class, 'register']);
    Route::get('noUsuarios', [AuthController::class, 'noUsuarios']);

    //CLIENTES
    Route::post('storeCliente', [ClienteController::class, 'store']);
    Route::get('clientesTabla', [ClienteController::class, 'clientesTabla']);
    Route::delete('/deleteCliente/{cliente}', [ClienteController::class, 'deleteCliente']);
    Route::get('/verCliente/{cliente}', [ClienteController::class, 'verCliente']);
    Route::put('actualizarCliente', [ClienteController::class, 'actualizarCliente']);

    //SUPERVISORES
    Route::get('crearVisita/{supervisor}', [SupervisorController::class, 'create']);
    Route::post('guardarVisita', [SupervisorController::class, 'store']);
    Route::get('visitasTabla/{supervisor}', [SupervisorController::class, 'visitasTabla']);
    Route::delete('/deleteVisita/{visita}', [SupervisorController::class, 'deleteVisita']);
    Route::post('/verVisita', [SupervisorController::class, 'verVisita']);
    Route::put('actualizarVisita', [SupervisorController::class, 'actualizarVisita']);

    //TECNICOS
    Route::post('visitasAsignadas', [TecnicoController::class, 'visitasAsignadas']);
    Route::get('/verVisitaTecnico', [TecnicoController::class, 'verVisitaTecnico']);
    Route::put('comenzarVisita', [TecnicoController::class, 'comenzarVisita']);
    Route::put('finalizarVisita', [TecnicoController::class, 'finalizarVisita']);
    Route::post('guardarlog', [TecnicoController::class, 'guardarlog']);
    Route::get('logsTabla/{visita_id}', [TecnicoController::class, 'logsTabla']);
    Route::get('/verLocalizacion/{visita_id}', [TecnicoController::class, 'verLocalizacion']);

    //EMPLEADOS
    Route::get('empleadosTabla', [EmpleadoController::class, 'empleadosTabla']);
    Route::post('guardarEmpleado', [EmpleadoController::class, 'guardarEmpleado']);
    Route::get('municipios', [EmpleadoController::class, 'municipios']);
});
