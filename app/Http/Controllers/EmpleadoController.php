<?php

namespace App\Http\Controllers;

use App\Models\Empleado;
use App\Models\EmpleadoPlaza;
use App\Models\Municipio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmpleadoController extends Controller
{
    public function empleadosTabla()
    {
        $empleados = Empleado::join('empleado_plaza as ep', 'ep.empleado_id', '=', 'empleado.id')
            ->join('area as a', 'a.id', '=', 'ep.area_id')
            ->join('plaza as p', 'p.id', '=', 'ep.plaza_id')
            ->select('empleado.fecha_nacimiento as fech', 'p.nombre as nombre_plaza', 'empleado.direccion',
                    'empleado.zona', 'empleado.activo', 'a.nombre as nombre_area', 'empleado.id',
                    DB::raw('CONCAT_WS(" ",empleado.primer_nombre, empleado.segundo_nombre, empleado.primer_apellido, empleado.segundo_apellido) as full_name'))
            ->get();

        return response()->json($empleados);
    }

    public function guardarEmpleado(Request $request)
    {
        $empleado = new Empleado();
        $empleado->primer_nombre = $request->primer_nombre;
        $empleado->segundo_nombre = $request->segundo_nombre;
        $empleado->primer_apellido = $request->primer_apellido;
        $empleado->segundo_apellido = $request->segundo_apellido;
        $empleado->fecha_nacimiento = $request->nacimiento;
        $empleado->cui = $request->dpi;
        $empleado->direccion = $request->dir;
        $empleado->zona = $request->zona;
        $empleado->municipio_id = $request->municipio;
        $saveEmpleado = $empleado->save();

        $plaza = new EmpleadoPlaza();
        $plaza->empleado_id = $empleado->id;
        $plaza->area_id = $request->area;
        $plaza->plaza_id = $request->plaza;
        $savePlaza = $plaza->save();

        if($saveEmpleado && $savePlaza){
            return response()->json([
                "status" => 1,
                "message" => "Empleado guardado exitosamente!",
            ], 200);
        }else{
            return response()->json([
                "status" => 0,
                "message" => "Error, Empleado no ha sido guardado.",
            ], 400);
        }
    }

    public function municipios()
    {
        $municipios = Municipio::all();
        return response()->json($municipios);
    }
}
