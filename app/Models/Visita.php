<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Visita extends Model
{
    use HasFactory;

    protected $table = 'visita';
    protected $primaryKey = 'id';
    public $timestamps = true;

    use SoftDeletes;
}
