<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LogVisita extends Model
{
    use HasFactory;

    protected $table = 'log_visita';
    protected $primaryKey = 'id';
    public $timestamps = true;

    use SoftDeletes;
}
