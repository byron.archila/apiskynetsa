<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use App\Models\Empleado;
use App\Models\Estado;
use App\Models\LogVisita;
use App\Models\Visita;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SupervisorController extends Controller
{
    public function create(Request $request)
    {
        $tecnicos = Empleado::join('empleado_plaza as ep', 'ep.empleado_id', '=', 'empleado.id')
            ->join('area as a', 'a.id', '=', 'ep.area_id')
            ->join('plaza as p', 'p.id', '=', 'ep.plaza_id')
            ->where('a.supervisor_id', $request->supervisor)
            ->where('p.id', 3) // Tecnicos
            ->where('empleado.activo', 1)
            ->select(DB::raw('CONCAT_WS(" ",empleado.primer_nombre, empleado.segundo_nombre, empleado.primer_apellido, empleado.segundo_apellido) as full_name'),
             'ep.id as empleado_plaza_id')
            ->get();

        $clientes = Cliente::whereNull('deleted_at')->get();

        $estados = Estado::where('activo', 1)->get();

        return response()->json([
            "clientes" => $clientes,
            "tecnicos" => $tecnicos,
            "estados" => $estados
        ]);
    }

    public function store(Request $request)
    {
        $visita = new Visita();
        $visita->descripcion = $request->descripcion;
        $visita->fecha_programada = $request->fecha;
        $visita->empleado_plaza_id = $request->tecnico_id;
        $visita->cliente_id = $request->cliente_id;
        $visita->estado_id = 1; //1 = Agendada
        $saveVisita = $visita->save();

        $logVisita = new LogVisita();
        $logVisita->comentario = 'Visita creada';
        $logVisita->actualizado_por = $request->auth_id;
        $logVisita->estado = 1;
        $logVisita->visita_id = $visita->id;
        $logVisita->save();

        if($saveVisita){
            return response()->json([
                "status" => 1,
                "message" => "Visita guardada exitosamente!",
            ], 200);
        }else{
            return response()->json([
                "status" => 0,
                "message" => "Error, Visita no ha sido guardada.",
            ], 400);
        }
    }

    public function visitasTabla(Request $request)
    {
        $visitas = Visita::join('empleado_plaza as ep', 'ep.id', '=', 'visita.empleado_plaza_id')
            ->join('empleado as emp', 'emp.id', '=', 'ep.empleado_id')
            ->join('cliente as c', 'c.id', '=', 'visita.cliente_id')
            ->join('area as a', 'a.id', '=', 'ep.area_id')
            ->where('a.supervisor_id', $request->supervisor)
            ->select(DB::raw('CONCAT_WS(" ",emp.primer_nombre, emp.segundo_nombre, emp.primer_apellido, emp.segundo_apellido) as full_name'),
                    'c.nombre', 'visita.*')
            ->orderBy('visita.fecha_programada', 'ASC')
            ->get();

        return response()->json($visitas);
    }

    public function deleteVisita(Visita $visita)
    {
        if($visita)
        {
            $visita->delete();
            return response()->json([
                "status" => 1,
                "message" => "Visita eliminada correctamente.",
            ], 200);
        }else{
            return response()->json([
                "status" => 0,
                "message" => "Error al eliminar Visita",
            ], 400);
        }
    }

    public function verVisita(Request $request)
    {
        $tecnicos = Empleado::join('empleado_plaza as ep', 'ep.empleado_id', '=', 'empleado.id')
            ->join('area as a', 'a.id', '=', 'ep.area_id')
            ->join('plaza as p', 'p.id', '=', 'ep.plaza_id')
            ->where('a.supervisor_id', $request->supervisor_id)
            ->where('p.id', 3) // Tecnicos
            ->select(DB::raw('CONCAT_WS(" ",empleado.primer_nombre, empleado.segundo_nombre, empleado.primer_apellido, empleado.segundo_apellido) as full_name'),
             'ep.id as empleado_plaza_id')
            ->get();

        $clientes = Cliente::whereNull('deleted_at')->get();

        $visita = Visita::find($request->id);

        return response()->json([
            "clientes" => $clientes,
            "tecnicos" => $tecnicos,
            "visita" => $visita
        ]);
    }

    public function actualizarVisita(Request $request)
    {
        $visita = Visita::find($request->id);
        $visita->descripcion = $request->descripcion;
        $visita->fecha_programada = $request->fecha;
        $visita->empleado_plaza_id = $request->tecnico_id;
        $visita->estado_id = 1; //1 = Agendada
        $saveVisita = $visita->save();

        if($saveVisita){
            return response()->json([
                "status" => 1,
                "message" => "Visita actualizada exitosamente!",
            ], 200);
        }else{
            return response()->json([
                "status" => 0,
                "message" => "Error, Visita no ha sido actualizada.",
            ], 400);
        }
    }
}
