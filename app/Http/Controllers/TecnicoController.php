<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use App\Models\Empleado;
use App\Models\LogVisita;
use App\Models\Visita;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TecnicoController extends Controller
{
    public function visitasAsignadas(Request $request)
    {
        $visitas = Visita::join('empleado_plaza as ep', 'ep.id', '=', 'visita.empleado_plaza_id')
            ->join('empleado as emp', 'emp.id', '=', 'ep.empleado_id')
            ->join('cliente as c', 'c.id', '=', 'visita.cliente_id')
            ->join('area as a', 'a.id', '=', 'ep.area_id')
            ->where('ep.empleado_id', $request->tecnico_id)
            ->select('c.direccion',
                    'c.nombre', 'visita.*');

        if($request->fecha == "" || $request->fecha == null){
            $visitas = $visitas->get();
        }else{
            $visitas = $visitas->where('visita.fecha_programada', $request->fecha)->get();
        }

        return response()->json($visitas);
    }

    public function verVisitaTecnico(Request $request)
    {
        $visita = Visita::join('cliente as c', 'c.id', '=', 'visita.cliente_id')
                ->where('visita.id', $request->id)
                ->select('visita.*', 'c.nombre as nombre_cliente', 'c.latitud', 'c.longitud')
                ->first();

        return response()->json([
            "visita" => $visita
        ]);
    }

    public function comenzarVisita(Request $request)
    {
        $visita = Visita::find($request->id);
        $visita->estado_id = 2; //En progreso
        $visita->save();

        $logVisita = new LogVisita();
        $logVisita->comentario = 'Inicio de la visita';
        $logVisita->actualizado_por = $request->auth_id;
        $logVisita->estado = 2;
        $logVisita->visita_id = $request->id;
        $savelogVisita = $logVisita->save();

        if($savelogVisita){
            return response()->json([
                "status" => 1,
                "message" => "Visita actualizada exitosamente!",
            ], 200);
        }else{
            return response()->json([
                "status" => 0,
                "message" => "Error, Visita no ha sido actualizada.",
            ], 400);
        }

    }

    public function finalizarVisita(Request $request)
    {
        $visita = Visita::find($request->id);
        $visita->estado_id = 3; // Completada
        $visita->save();

        $logVisita = new LogVisita();
        $logVisita->comentario = 'Fin de la visita';
        $logVisita->actualizado_por = $request->auth_id;
        $logVisita->estado = 3;
        $logVisita->visita_id = $request->id;
        $savelogVisita = $logVisita->save();

        /*
        Email::send([
            emailViewReport,
            Visita Finalizada
        ])->to('byron@gamil.com')
        */

        if($savelogVisita){
            return response()->json([
                "status" => 1,
                "message" => "Visita finalizada exitosamente!",
            ], 200);
        }else{
            return response()->json([
                "status" => 0,
                "message" => "Error, Visita no ha sido actualizada.",
            ], 400);
        }
    }

    public function guardarlog(Request $request)
    {
        $logVisita = new LogVisita();
        $logVisita->comentario = $request->comentario;
        $logVisita->actualizado_por = $request->auth_id;
        $logVisita->estado = 2;
        $logVisita->visita_id = $request->id;
        $savelogVisita = $logVisita->save();

        if($savelogVisita){
            return response()->json([
                "status" => 1,
                "message" => "Comentario guardado exitosamente!",
            ], 200);
        }else{
            return response()->json([
                "status" => 0,
                "message" => "Error, comentario no ha sido guardado.",
            ], 400);
        }

    }

    public function logsTabla(Request $request)
    {
        $logs = LogVisita::where('visita_id', $request->visita_id)
        ->select('comentario', 'estado', DB::raw("DATE_FORMAT(created_at, '%d/%m/%Y %H:%i:%s') as date"))
            ->orderBy('date', 'asc')
            ->get();

        return response()->json($logs);
    }

    public function verLocalizacion(Request $request)
    {
        $cliente = Visita::join('cliente as c', 'c.id', '=', 'visita.cliente_id')
        ->where('visita.id', $request->visita_id)
        ->select('c.*')
        ->first();

        return response()->json($cliente);
    }
}
