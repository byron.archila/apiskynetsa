<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use Illuminate\Http\Request;

class ClienteController extends Controller
{
    public function store(Request $request)
    {
        $cliente = new Cliente();
        $cliente->nombre = $request->nombre;
        $cliente->email = $request->email;
        $cliente->telefono = $request->telefono;
        $cliente->direccion = $request->direccion;
        $cliente->latitud = $request->latitud;
        $cliente->longitud = $request->longitud;
        $cliente->descripcion = $request->descripcion;
        $saveCliente = $cliente->save();

        if($saveCliente){
            return response()->json([
                "status" => 1,
                "message" => "Cliente guardado exitosamente!",
            ], 200);
        }else{
            return response()->json([
                "status" => 0,
                "message" => "Error, cliente no ha sido guardado.",
            ], 400);
        }
    }

    public function clientesTabla()
    {
        $clientes = Cliente::whereNull('deleted_at')->get();

        return response()->json($clientes);
    }

    public function deleteCliente(Cliente $cliente)
    {
        if($cliente)
        {
            $cliente->delete();
            return response()->json([
                "status" => 1,
                "message" => "Cliente eliminado correctamente.",
            ], 200);
        }else{
            return response()->json([
                "status" => 0,
                "message" => "Error al eliminar cliente",
            ], 400);
        }
    }

    public function verCliente(Cliente $cliente)
    {
        $cliente = Cliente::find($cliente->id);

        return response()->json($cliente);
    }

    public function actualizarCliente(Request $request)
    {
        $cliente = Cliente::find($request->id);
        $cliente->nombre = $request->nombre;
        $cliente->email = $request->email;
        $cliente->activo = $request->estado;
        $cliente->telefono = $request->telefono;
        $cliente->descripcion = $request->descripcion;
        $cliente->direccion = $request->direccion;
        $cliente->latitud = $request->latitud;
        $cliente->longitud = $request->longitud;
        $saveCliente = $cliente->save();

        if($saveCliente){
            return response()->json([
                "status" => 1,
                "message" => "Cliente actualizado exitosamente!",
            ], 200);
        }else{
            return response()->json([
                "status" => 0,
                "message" => "Error, cliente no ha sido actualizado.",
            ], 400);
        }
    }
}
